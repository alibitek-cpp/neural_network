#include <cmath>
#include <random>
#include <iostream>

namespace neural_networks
{
  namespace activation_functions
  {
    /**
    * ReLU (Rectified Linear Unit)
    */
    double constexpr relu(double x) noexcept
    {
      return x >= 0 ? x : 0.0;
    }

    /**
    * Leaky ReLu
    */
    double constexpr leaky_relu(double x) noexcept
    {
      return x >= 0 ? x : (x / 100.0);
    }
    
    /**
     * Logistic Sigmoid
     */
    double constexpr sigmoid(double x) noexcept
    {
      return 1 / (1 + exp(-x));
    }
  }
  
  double constexpr error(double actualOutput, double expectedOutput)
  {
    return actualOutput - expectedOutput;
  }
  
  struct Neuron
  {
    double weight1, weight2;
    double bias;
    
    Neuron()
      : weight1(GetSmallRandomNumber()),
        weight2(GetSmallRandomNumber()),
        bias(GetSmallRandomNumber())
    {
    }
    
    static double GetSmallRandomNumber()
    {
      static std::random_device rd;
      static std::mt19937 gen(rd());
      static std::uniform_real_distribution<> dis_real(0, 1);
      static std::uniform_int_distribution<> dis_int(0, 1);

      return (0.0009 * dis_real(gen) + 0.001) * ((dis_int(gen) == 0) ? -1 : 1);
    }
    
    void dump(const std::string& str)
    {
      std::cout 
           << str << "\n" 
           << "Weight1: " << weight1 << "\n"
           << "Weight2: " << weight2 << "\n"
           << "Bias: " << bias << "\n";
    }
  };
  
  struct FiringNeuron
  {
    Neuron neuron;
    double totalInput, output;
    
    FiringNeuron(Neuron neuron)
      : neuron(neuron)
    {
    }
    
    double fire(double input1, double input2)
    {
      totalInput = (input1 * neuron.weight1) + 
                   (input2 * neuron.weight2) + 
                   neuron.bias;
                 
      // Apply ReLU
      output = activation_functions::leaky_relu(totalInput);
      
      return output;
    }
    
    void learn(double input1, double input2, 
               double expectedOutput, double learningRate)
    {
      fire(input1, input2);
      
      // The loss is (Output - expectedOutput) * (Output - expectedOutput) / 2
      // When we derive this we get (Output - expectedOutput).
      // Sign is reversed because positive gradient means move left, negative gradient means move right.
      double outputVotes = error(expectedOutput, output);
      
      // apply the chain rule: multiply by the slope of the ReLu function
      double slopeOfRelu = totalInput >= 0 ? 1 : 0.01;
      double inputVotes = outputVotes * slopeOfRelu;
      
      double adjustment = inputVotes * learningRate;
      
      neuron.bias += adjustment;
      neuron.weight1 += adjustment * input1;
      neuron.weight2 += adjustment * input2;
    }
  };
} 
