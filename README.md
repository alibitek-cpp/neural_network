# neural_networks

Neural networks implementation in C++.

## What?
Building a neural network that learns to perform the function of a NAND or NOR logical gate.

## Why?
Because the best way to understand neural networks is to get your hands dirty and write one.  
Implementing logic gates using neural networks helps to understand the mathematical computations   
by which a neural network processes its inputs to a arrive at a certain output.  

To learn more about:  
- Stochastic gradient descent  
- Activation functions  
- Backpropagation 

## How?
Without using any libraries. From the bottom up.

## Test
`g++ neural_network_main.cpp && ./a.out`

### Sample output for NAND gate
```
totalSamples: 1000
learningRate: 0.02

Final state of neuron:
Weight1: -0.00148746
Weight2: -0.00114985
Bias: 0.00164013

Test results - summary
Successful      Count
True            200
Total           200

Average error magnitude: 0.25025

Error histogram
Error           Count
-0.33           41
-0.24           68
0.17            38
0.25            53
Total error: 
-0.15           200
```

Less samples for training (no longer works):
```
totalSamples: 100
learningRate: 0.02

Final state of neuron:
Weight1: -0.00154017
Weight2: -0.00166772
Bias: 0.00139999

Test results - summary
Successful      Count
False           8
True            12
Total           20

Average error magnitude: 0.484117

Error histogram
Error           Count
-0.51           3
-0.49           5
-0.38           7
0.61            5
Total error: 
-0.77           20
```

Learning rate too big (no longer works):
```
totalSamples: 1000
learningRate: 0.5

Final state of neuron:
Weight1: -0.00165055
Weight2: 0.00146208
Bias: 0.00161146

Test results - summary
Successful      Count
False           50
True            150
Total           200

Average error magnitude: 0.335153

Error histogram
Error           Count
0               59
0.17            36
0.45            55
0.72            50
Total error: 
1.34            200
```

### Sample output for NOR gate
```
totalSamples: 1000
learningRate: 0.02

Final state of neuron:
Weight1: -0.00104843
Weight2: -0.00148689
Bias: 0.00176432

Test results - summary
Successful      Count
True            200
Total           200

Average error magnitude: 0.254149

Error histogram
Error           Count
-0.26           50
-0.24           55
0.17            42
0.33            53
Total error: 
5.55112e-17             200
```
