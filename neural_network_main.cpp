#include "neural_networks.h"
#include <cmath>
#include <cstdint>
#include <random>
#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <map>

void test_activation_functions()
{
    std::cout 
      << neural_networks::activation_functions::sigmoid(0.122) << "\n"
      << neural_networks::activation_functions::leaky_relu(0.3) << "\n"
      << neural_networks::error(0.3, 1)
    << std::endl;
}

struct Data
{
  double input1;
  double input2;
  double desiredOutput;
};

typedef std::vector<Data> DataType;

std::ostream& operator<<(std::ostream& ostream, DataType data)
{
  ostream << "input1" << " " << "input2" << " " << "DesiredOutput" << "\n";
  for (const auto& item : data)
  {
    ostream << item.input1 << ' ' << item.input2 << ' ' << item.desiredOutput << '\n';
  }
  
  return ostream;
}

double round2nplaces(double value, int to)
{
    double places = std::pow(10.0, to);
    return std::round(value * places) / places;
}

int main(int argc, char* argv[])
{    
  const int totalSamples = 1000; // variations: less samples: 100
  const double learningRate = 0.02; // variations: faster learning rate: 0.5
  
  DataType data(totalSamples);
  
  std::random_device rd;
  std::mt19937 gen(rd());
  // std::uniform_real_distribution<> dis(0, 2);
  std::uniform_int_distribution<> dis(0, 1);
  
  for (int i = 0; i < totalSamples; ++i)
  {
    double input1 = dis(gen),
           input2 = dis(gen),
           // A NAND gate, 0 only when both inputs are 1: https://en.wikipedia.org/wiki/NAND_gate
           desiredOutput = ((input1 == 1 && input2 == 1) ? 0 : 1)
           
           // A NOR gate, 1 only when boths inputs are 0: https://en.wikipedia.org/wiki/NOR_gate
           //desiredOutput = ((input1 == 0 || input2 == 0) ? 1 : 0)
           
           // A XOR gate (doesn't work, because it needs more than one neuron, we need to feed the output of a neuron to the input of another neuron)
           //desiredOutput = ((input1 == 1 ^ input2 == 1) ? 0 : 1)
           ;
    //std::cout << "input1: " << input1 << " input2: " << input2 << " desiredOutput: " << desiredOutput << "\n";
    data[i] = Data { input1, input2, desiredOutput };
  }
  
  //std::cout << "Data: \n" << data;
  
  // Split the data into training and testing sets
  constexpr int trainingCount = (totalSamples * 8) / 10;
  
  //std::cout << "Training count: " << trainingCount << '\n';
  
  // Training: 80% of the data
  DataType trainingSet(data.begin(), data.begin() + trainingCount);
  //std::cout << trainingSet << "Training set count: " << trainingSet.size() << '\n';
  
  // Testing: 20% of the data
  DataType testingSet(data.begin() + trainingCount, data.end());
  //std::cout << testingSet << "Testing set count: " << testingSet.size() << '\n';
  
  neural_networks::Neuron neuron;
  neural_networks::FiringNeuron firingNeuron(neuron);
  
  // Train
  for (const auto& sample : trainingSet)
  {
    firingNeuron.learn(sample.input1, sample.input2, 
                       sample.desiredOutput, learningRate);
  }
  
  // Test
  typedef std::unordered_map<bool, std::vector<double>> TestResultsType;
  TestResultsType testResults;
  testResults.reserve(testingSet.size());
  
  for (const auto& sample : testingSet)
  {
    double actualOutput = firingNeuron.fire(sample.input1, sample.input2);
    bool success = ((actualOutput >= 0.5) == (sample.desiredOutput == 1));
    double error = neural_networks::error(actualOutput, sample.desiredOutput);
    testResults[success].push_back(error);
  }
  
  // Report results
  neuron.dump("\nFinal state of neuron:");
  
  std::cout << "\nTest results - summary\n";
  std::size_t totalTestResultsCount = 0;
  std::cout << "Successful\tCount\n";
  for (const TestResultsType::value_type& testResult : testResults)
  {
    bool successful = testResult.first;
    std::size_t count = testResult.second.size();
    totalTestResultsCount += count;
    std::cout << (successful ? "True" : "False") << "\t\t" << count << "\n";
  }
  std::cout << "Total\t\t" << totalTestResultsCount << "\n\n";
  
  std::vector<double> errors;
  errors.reserve(testingSet.size());
  
  for (const TestResultsType::value_type& testResult : testResults)
  {
    for (const double& error : testResult.second)
    {
      errors.push_back(error);
    }
  }
	
  double averageErrorMagnitude = std::accumulate(errors.cbegin(), errors.cend(), 0.0, 
					         [](double sum, double value) { return sum + std::fabs(value); }) / errors.size(); 
  std::cout << "Average error magnitude: " << averageErrorMagnitude << "\n\n";
  
  
  std::cout << "Error histogram\n";
  
  typedef std::map<double, std::vector<std::uint16_t>> LossesType;
  LossesType losess;
    
  for (const double& error : errors)
  {
    double loss = round2nplaces(error, 2);
    losess[loss].push_back(1);
  }
  
  std::cout << "Error\t\t" << "Count\n";
  double lossesSum = 0.0;
  std::size_t lossesCount = 0;
  for (const LossesType::value_type& loss : losess)
  {
    double lossValue = loss.first;
    lossesSum += lossValue;
    
    std::size_t countValue = loss.second.size();
    lossesCount += countValue;
    
    std::cout << lossValue << "\t\t" << countValue << "\n";
  }
  std::cout << "Total error: \n" << lossesSum << "\t\t" << lossesCount << "\n";
    
  return 0;
}
